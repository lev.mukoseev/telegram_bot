package bot;

import game.Game;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import schedule.Schedule;


public class Bot extends TelegramLongPollingBot {
    public static void main(String[] args) {
        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try{
            telegramBotsApi.registerBot(new Bot());

        } catch(TelegramApiException e){
            e.printStackTrace();
        }

    }

    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();
        if(message != null && message.hasText()){
            String text = message.getText();

            if (text.equals("/help"))
            {
                var response = "/info – shows schedule and weather for current day \n" +
                               "/schedule – finds schedule of your group for next three days \n" +
                               "/weather tells about weekly weather in your area \n" +
                               "/changeInfo sets you a group and city or changes current information ";
                sendMsg(message, response, true);
            }
            else if (text.equals("/changeInfo"))
            {
                sendMsg(message, "What can I change for you?", true);
            }
            else if (text.matches("/schedule .*")) {
                sendMsg(message, Schedule.getSchedule(text), false);
            }
            else if (text.matches("/game")) {
                Game game = new Game();
                System.out.println("Start anagrams game");
                game.run(this);
            }
            else
            {
                sendMsg(message, "Misunderstandings happen, try again or write /help");
            }
        }
    }


    private void sendMsg(Message message, String text, boolean isReply) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId().toString());
        if (isReply)
            sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText(text);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void sendMsg(Message message, String text){
        sendMsg(message, text, false);
    }

    public String getBotUsername() {
        return "java_project";
    }

    public String getBotToken() {
        return "815206226:AAGdEge7flJz5L1JKUM2t8X3DoQloKFWsro";
    }



}
