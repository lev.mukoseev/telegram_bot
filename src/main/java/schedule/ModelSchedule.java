package schedule;

import java.util.HashMap;

public class ModelSchedule {
    private String groupName;
    private String[] days;
    private HashMap<String, Lecture[]> schedule = new HashMap<String, Lecture[]>() {};

    public HashMap<String, Lecture[]> getSchedule() {
        return schedule;
    }

    public String[] getDays() {
        return days;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setSchedule(String day, Lecture[] lectures) {
        this.schedule.put(day, lectures);
    }

    public void setDays(String[] days) {
        this.days = days;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String toString()
    {
        var result = new StringBuilder();
        result.append(this.groupName).append("\n");
        for (String day : days) {
            result.append("-".repeat(40)).append("\n").append(day).append(": \n").append("-".repeat(40)).append("\n");
            for (Lecture lecture: schedule.get(day)) {
                result.append(lecture.toString());
                result.append("\n");
            }
        }
        return result.toString();
    }
}
