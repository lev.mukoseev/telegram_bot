package schedule;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Schedule {
    private static String schedule;

    private static ModelSchedule scheduleParser()
    {
        var count = 3;
        var result = new ModelSchedule();
        result.setGroupName(getGroupName());
        result.setDays(getDays(count));
        for (String day: result.getDays()) {
            result.setSchedule(day, getLectures(day));
        }
        return result;
    }

    private static String getGroupName()
    {
        var groupPattern = Pattern.compile("Группа [А-Я]{3}-\\d{6}");
        var matcher = groupPattern.matcher(schedule);
        matcher.find();
        return schedule.substring(matcher.start(), matcher.end());
    }

    private static String[] getDays(int count)
    {
        var days = new String[count];
        var daysPattern = Pattern.compile("<b>([\\d\\sа-яА-Я]+?)</b>");
        var matcher = daysPattern.matcher(schedule);
        while(matcher.find() && count > 0){
            days[days.length-count] = matcher.group(1);
            count--;
        }
        return days;
    }

    private static Lecture[] getLectures(String day)
    {
        var result = new ArrayList<Lecture>();
        var start = schedule.indexOf("<b>" + day + "</b>");
        var end = schedule.substring(start + day.length()).indexOf("<b>") + start;
        var scheduleDay = schedule.substring(start, end);
        var timeMatcher = Pattern.compile("<td class=\"shedule-weekday-time\">(.+?)</td>").matcher(scheduleDay);
        var nameMatcher = Pattern.compile("<dd>(.+?)</dd>", Pattern.DOTALL | Pattern.MULTILINE)
                .matcher(scheduleDay);
        var cabinetMatcher = Pattern.compile(
                "<span class=\"cabinet\">(.+?)</span>",
                Pattern.DOTALL| Pattern.MULTILINE)
                    .matcher(scheduleDay);
        var teacherMatcher = Pattern.compile("<span class=\"teacher\">Преподаватель:(.+?)</span>").matcher(scheduleDay);
        while (nameMatcher.find() && timeMatcher.find() && cabinetMatcher.find() && teacherMatcher.find())
        {
            var name = nameMatcher.group(1);
            var time = timeMatcher.group(1);
            var cabinet = cabinetMatcher.group(1);
            var teacher = teacherMatcher.group(1);
            result.add(new Lecture(name, teacher, time, cabinet));
        }
        return result.toArray(new Lecture[0]);
    }

    public static String getSchedule(String message)
    {
        var groupMatcher = Pattern.compile("/schedule (.*)").matcher(message);
        groupMatcher.find();
        var group = groupMatcher.group(1);
        var groupID = getGETRequestContent(
                "https://urfu.ru/api/schedule/groups/suggest/?query=" + URLEncoder.encode(group));
        int index = groupID.indexOf("\"data\":")+8;
        groupID = groupID.substring(index, index+6);
        System.out.println(groupID);
        schedule = getGETRequestContent("https://urfu.ru/api/schedule/groups/lessons/" + groupID);
        var result = scheduleParser();
        return result.toString();
    }

    private static String getGETRequestContent(String url)
    {
        try
        {
            var urfuSchedulePage = new URL(url);
            var request = (HttpURLConnection) urfuSchedulePage.openConnection();
            request.setRequestProperty(
                    "User-Agent",
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko)"
                            + " Chrome/77.0.3865.90 Safari/537.36");
            request.setRequestMethod("GET");
            Scanner input = new Scanner((InputStream) request.getContent());
            StringBuilder result = new StringBuilder();
            while (input.hasNext())
                result.append(input.nextLine());
            return result.toString();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "";
        }
    }


}
