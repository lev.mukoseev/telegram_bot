package weather;

import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

class Weather {
    static String getWeather(String message, ModelWeather modelWeather){
        try {
            URL url = new URL("https://api.openweathermap.org/data/2.5/weather?q="+message+"&units=metric&appid=e0a1590c3b0385e8e50f67972a79bc76");
            Scanner input = new Scanner((InputStream)url.getContent());
            StringBuilder result = new StringBuilder();
            while (input.hasNext())
                result.append(input.nextLine());
            return result.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "Город не найден";
        }
    }
}
